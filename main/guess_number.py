"""guess number"""
import random


def guess_int(start, stop):
    """
test guess int.
"""
    return random.randint(start, stop)


def guess_float(start, stop):
    """
test guess float.
"""
    return random.uniform(start, stop)
